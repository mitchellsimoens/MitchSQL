import { Component, Event, EventEmitter, State } from '@stencil/core'
const { ipcRenderer } = window.require('electron')

@Component({
  shadow: true,
  styleUrl: 'app-connections.scss',
  tag: 'app-connections'
})
export class AppConnections {
  @Event() public connectionForm: EventEmitter
  @Event() public showDialog: EventEmitter

  @State() private connections?: any

  public render (): JSX.Element {
    const { connections } = this

    const list = connections
      ? Object
        .keys(connections)
        .map((type: string): any => this.renderType(type, connections[ type ]))
      : null

    return (
      <div>
        <div class='top-dock'>
          <span onClick={this.handleAddConnectionClick.bind(this)}>
            <ion-icon name='add' />
            Add Connection
          </span>
        </div>

        <div class='body'>
          {list}
        </div>
      </div>
    )
  }

  public componentDidLoad (): void {
    ipcRenderer.once('get-connections-result', this.onConnections)
    ipcRenderer.send('get-connections')
  }

  private handleAddConnectionClick (): void {
    this.showDialog.emit({
      items: [
        <connection-form></connection-form>
      ],
      size: 'small',
      title: 'Add Connection'
    })
  }

  private onConnections = (_event: Event, data: any): void => {
    this.connections = data
  }

  private renderType (type: string, connections: any): JSX.Element {
    const list = Object
      .keys(connections)
      .map((alias: string): any => this.renderAlias(type, alias, connections[ alias ]))

    return <div>{list}</div>
  }

  private renderAlias (type: string, alias: string, data: any): JSX.Element {
    return <app-connection
      alias={alias}
      data={data}
      type={type}
      />
  }
}
