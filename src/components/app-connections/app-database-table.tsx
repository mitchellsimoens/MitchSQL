import { Component, Event, EventEmitter, Prop } from '@stencil/core'
const { remote } = window.require('electron')

const { app: { isPackaged }, Menu, MenuItem } = remote

@Component({
  shadow: true,
  styleUrl: 'app-database-table.scss',
  tag: 'app-database-table'
})
export class AppDatabaseTable {
  @Event() private handleTab: EventEmitter

  @Prop() public alias: string
  @Prop() public connection: any
  @Prop() public data: any
  @Prop() public database: any
  @Prop() public type: string

  private onContextMenu = (e: MouseEvent): void => {
    e.preventDefault()
    e.stopPropagation()

    const { alias, connection, data, database, handleTab, type } = this

    const items = [
      {
        click (): void {
          handleTab.emit({
            action: 'SELECT_FIRST_100_ROWS',
            config: {
              alias,
              connection,
              database,
              type,
              table: data
            }
          })
        },
        label: 'Select First 100 Rows'
      },
      {
        label: 'Truncate',
        submenu: [
          {
            label: 'Truncate'
          },
          {
            label: 'Truncate Cascade'
          }
        ]
      },
      new MenuItem({ type: 'separator' }),
      {
        label: 'Edit Table'
      },
      {
        label: 'Drop Table'
      }
    ]

    if (!isPackaged) {
      const win = remote.getCurrentWindow()

      items.push(
        new MenuItem({ type: 'separator' }),
        new MenuItem({
          click (): void {
            win.focus()
            win.devToolsWebContents.focus()

            win.inspectElement(e.x, e.y)
          },
          label: 'Inspect Element'
        })
      )
    }

    const menu = Menu.buildFromTemplate(items)

    menu.popup({
      window: remote.getCurrentWindow()
    })
  }

  public render(): JSX.Element {
    const { alias, database, data, type } = this

    return <div
      onContextMenu={this.onContextMenu}
      db-type={type}
      db-alias={alias}
      db-database={database.name}
      db-table={data.name}
    >{data.name}</div>
  }
}
