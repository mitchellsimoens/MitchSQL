import { Component, Element, Event, EventEmitter, Listen, Prop } from '@stencil/core'
const { ipcRenderer, remote } = window.require('electron')

const { app: { isPackaged }, Menu, MenuItem } = remote

@Component({
  shadow: true,
  styleUrl: 'app-connection.scss',
  tag: 'app-connection'
})
export class AppConnection {
  @Element() private el!: HTMLStencilElement

  @Event() public showDialog: EventEmitter

  @Prop() public alias: string
  @Prop() public data: any
  @Prop() public type: string

  private onContextMenu = (e: MouseEvent): void => {
    e.preventDefault()
    e.stopPropagation()

    const items = [
      {
        click: this.createSchema,
        label: 'Create Schema'
      },
      new MenuItem({ type: 'separator' }),
      {
        label: 'Edit Connection'
      },
      {
        label: 'Remove Connection'
      }
    ]

    if (!isPackaged) {
      items.push(
        new MenuItem({ type: 'separator' }),
        new MenuItem({
          click: this.inspectElement.bind(this, e),
          label: 'Inspect Element'
        })
      )
    }

    const menu = Menu.buildFromTemplate(items)

    menu.popup({
      window: remote.getCurrentWindow()
    })
  }

  private onClick = (): void => {
    const { alias, data, type } = this

    if (!data.meta) {
      data.meta = {}
    }

    data.meta.expanded = !data.meta.expanded

    if (!data.meta.databases) {
      ipcRenderer.once('get-connection-databases-result', this.onDatabases)
      ipcRenderer.send('get-connection-databases', { alias, type })
    }

    this.el.forceUpdate()
  }

  private onDatabases = (_event: Event, result: any): void => {
    const { data } = this

    if (result.success) {
      if (!data.meta) {
        data.meta = {}
      }

      data.meta.databases = result.data

      this.el.forceUpdate()
    } else {
      // TODO handle
    }
  }

  public render(): JSX.Element {
    const { alias, data, type } = this
    const { meta }: any = data
    const expanded = meta && meta.expanded
    const icon = expanded ? 'arrow-dropdown' : 'arrow-dropright'
    const databases = expanded
      ? this.renderDatabases()
      : null

    return (
      <div db-type={type} db-alias={alias}>
        <div
          onClick={this.onClick}
          onContextMenu={this.onContextMenu}
        >
          <ion-icon name={icon} />
          <strong>{alias} ({type})</strong>
        </div>
        {databases}
      </div>
    )
  }

  private renderDatabases(): JSX.Element {
    const { data } = this
    const { meta } = data

    const list = meta && meta.databases
      ? <div>{this.renderDatabaseList()}</div>
      : <div>Loading...</div>

    return <div class='indented'>{list}</div>
  }

  private renderDatabaseList (): JSX.Element[] {
    const { alias, data, type } = this
    const { databases } = data.meta

    return databases.map((database: any): JSX.Element =>
        <app-database
          alias={alias}
          connection={data}
          data={database}
          type={type}
          />
    )
  }

  private inspectElement = (e): void => {
    const win = remote.getCurrentWindow()

    win.focus()
    win.devToolsWebContents.focus()

    win.inspectElement(e.x, e.y)
  }

  private createSchema = (): void => {
    this.showDialog.emit({
      items: [
        <create-schema alias={this.alias} type={this.type}></create-schema>
      ],
      size: 'small',
      title: 'Create Schema'
    })
  }

  @Listen('body:databaseEvent')
  public onDatabaseEvent (event: any): void {
    const { detail: { action, alias, name, type } } = event

    if (alias === this.alias && type === this.type) {
      const { data: { meta } } = this

      if (action === 'add') {
        if (meta && meta.databases) {
          meta.databases.push({
            name
          })

          this.el.forceUpdate()
        }
      } else if (action === 'drop') {
        if (meta && meta.databases) {
          const newDatabaseArray = meta
            .databases
            .slice()
            .filter((database: any) => database.name !== name)

          meta.databases = newDatabaseArray

          this.el.forceUpdate()
        }
      }
    }
  }
}
