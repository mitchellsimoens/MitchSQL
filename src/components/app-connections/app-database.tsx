import { Component, Element, Event, EventEmitter, Listen, Prop } from '@stencil/core'
import { Event as ElectronEvent } from 'electron'
const { ipcRenderer, remote } = window.require('electron')

const { app: { isPackaged }, Menu, MenuItem } = remote

@Component({
  shadow: true,
  styleUrl: 'app-database.scss',
  tag: 'app-database'
})
export class AppDatabase {
  @Element() private el!: HTMLStencilElement

  @Event() public databaseEvent: EventEmitter
  @Event() public showDialog: EventEmitter

  @Prop() public alias: string
  @Prop() public connection: any
  @Prop() public data: any
  @Prop() public type: string

  private onClick = (): void => {
    const { alias, data, type } = this

    if (!data.meta) {
      data.meta = {}
    }

    data.meta.expanded = !data.meta.expanded

    if (!data.meta.tables) {
      ipcRenderer.once('get-connection-tables-result', this.onTables)
      ipcRenderer.send('get-connection-tables', {
        alias, type,
        database: data.name
      })
    }

    this.el.forceUpdate()
  }

  private onContextMenu = (e: MouseEvent): void => {
    e.preventDefault()
    e.stopPropagation()

    const items = [
      {
        click: this.createTable,
        label: 'Create Table'
      },
      new MenuItem({ type: 'separator' }),
      {
        label: 'Edit Database'
      },
      {
        click: this.dropDatabase,
        label: 'Drop Database'
      }
    ]

    if (!isPackaged) {
      items.push(
        new MenuItem({ type: 'separator' }),
        new MenuItem({
          click: this.inspectElement,
          label: 'Inspect Element'
        })
      )
    }

    const menu = Menu.buildFromTemplate(items)

    menu.popup({
      window: remote.getCurrentWindow()
    })
  }

  private onTables = (_event: Event, result: any): void => {
    const { data } = this

    if (result.success) {
      if (!data.meta) {
        data.meta = {}
      }

      data.meta.tables = result.data

      this.el.forceUpdate()
    } else {
      // TODO handle
    }
  }

  public render(): JSX.Element {
    const { alias, data, type } = this
    const { meta } = data
    const expanded = meta && meta.expanded
    const icon = expanded ? 'arrow-dropdown' : 'arrow-dropright'
    const children = expanded
      ? this.renderDatabaseTables()
      : null

      return (
        <div db-type={type} db-alias={alias} db-database={data.name}>
          <div
            onClick={this.onClick}
            onContextMenu={this.onContextMenu}
          >
            <ion-icon name={icon} />
            {data.name}
          </div>
          {children}
        </div>
      )
  }

  private renderDatabaseTables (): JSX.Element {
    const { data } = this
    const { meta }: any = data
    const list = meta && meta.tables
      ? this.renderTableList()
      : null

    return <div class='indented'>{list}</div>
  }


  private renderTableList (): JSX.Element[] {
    const { alias, connection, data, type } = this
    const { meta } = data
    const { tables } = meta

    return tables.map((table: any): JSX.Element =>
      <app-database-table
        alias={alias}
        connection={connection}
        database={data}
        data={table}
        type={type}
        />
    )
  }

  private inspectElement = (e): void => {
    const win = remote.getCurrentWindow()

    win.focus()
    win.devToolsWebContents.focus()

    win.inspectElement(e.x, e.y)
  }

  private dropDatabase = (): void => {
    const { alias, data, type } = this

    ipcRenderer.once('drop-schema-result', (_event: ElectronEvent, result: any) => {
      if (result.success) {
        this.databaseEvent.emit({
          alias, type,
          action: 'drop',
          name: data.name
        });
      }
    })

    ipcRenderer.send('drop-schema', {
      alias, type,
      name: data.name
    })
  }

  private createTable = (): void => {
    this.showDialog.emit({
      items: [
        <create-table alias={this.alias} database={this.data.name} type={this.type}></create-table>
      ],
      size: 'large',
      title: 'Create Table'
    })
  }

  @Listen('body:tableEvent')
  public onTableEvent (event: any): void {
    const { detail: { action, alias, database, name, type } } = event

    if (alias === this.alias && type === this.type && database === this.data.name) {
      const { data: { meta } } = this

      if (action === 'add') {
        if (meta && meta.tables) {
          meta.tables.push({
            name
          })

          this.el.forceUpdate()
        }
      } else if (action === 'drop') {
        if (meta && meta.tables) {
          const newTableArray = meta
            .tables
            .slice()
            .filter((table: any) => table.name !== name)

          meta.tables = newTableArray

          this.el.forceUpdate()
        }
      }
    }
  }
}
