import { Component, Element, Event, EventEmitter, State } from '@stencil/core'
import { Event as ElectronEvent } from 'electron'
const { ipcRenderer } = window.require('electron')

const defaultPorts = {
  mysql: '3306',
  pg: '5432'
}

@Component({
  shadow: true,
  styleUrl: 'connection-form.scss',
  tag: 'connection-form'
})
export class ConnectionForm {
  @Element() private host: HTMLElement

  @Event() public showDialog: EventEmitter
  @Event() public showDialogMask: EventEmitter

  @State() private typeValue: string = 'mysql'
  @State() private aliasValue: string
  @State() private hostValue: string = 'localhost'
  @State() private portValue: string
  @State() private portPlaceholder: string = defaultPorts.mysql
  @State() private usernameValue: string
  @State() private passwordValue: string
  @State() private databaseValue: string

  private focusNext: boolean = false

  public render(): JSX.Element {
    return <form class='inner'>
      <label>
        <div>Type</div>
        <select name='type' onChange={this.handleChange}>
          <option value='mysql' selected={this.typeValue === 'mysql'}>MySQL</option>
          <option value='pg' selected={this.typeValue === 'pg'}>PostgreSQL</option>
        </select>
      </label>
      <label>
        <div>Alias</div>
        <input type='text' name='alias' value={this.aliasValue} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>
      <label>
        <div>Server Host</div>
        <input type='text' name='host' value={this.hostValue} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>
      <label>
        <div>Port</div>
        <input type='number' name='port' value={this.portValue} placeholder={this.portPlaceholder} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>
      <label>
        <div>Username</div>
        <input type='text' name='username' value={this.usernameValue} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>
      <label>
        <div>Password</div>
        <input type='password' name='password' value={this.passwordValue} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>
      <label>
        <div>Database</div>
        <input type='text' name='database' value={this.databaseValue} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>

      <div class='toolbar'>
        <button type='button' onClick={this.save}>Save</button>
        <button type='button' onClick={this.hide}>Cancel</button>
      </div>
    </form>
  }

  public componentDidUpdate(): void {
    if (this.focusNext) {
      const field = this.host.shadowRoot.querySelector('select')

      field.focus()

      this.focusNext = false
    }
  }

  private hide = (): void => {
    this.showDialog.emit(false)
  }

  private handleChange = (event: any): void => {
    const { name, value } = event.target

    if (name === 'type') {
      const { portValue, typeValue } = this

      this.portPlaceholder = defaultPorts[ value ]

      if (!portValue || portValue === defaultPorts[ typeValue ]) {
        // only change it to default if no port exists or is on the
        // default port of the old type before it changed
        this.portValue = ''
      }
    }

    this[ `${name}Value` ] = value
  }

  private save = (event: KeyboardEvent | MouseEvent): void => {
    event.preventDefault()

    const {
      aliasValue,
      databaseValue,
      hostValue,
      passwordValue,
      portValue,
      typeValue,
      usernameValue
    } = this

    if (aliasValue && hostValue && passwordValue && typeValue && usernameValue) {
      this.showDialogMask.emit('Saving...')

      const data = {
        alias: aliasValue,
        database: databaseValue ? databaseValue : undefined,
        host: hostValue,
        password: passwordValue,
        port: portValue ? portValue : undefined,
        type: typeValue,
        username: usernameValue
      }

      ipcRenderer.once('add-connect-result', (_event: ElectronEvent, result: any) => {
        if (result.success) {
          this.showDialog.emit(false)
        } else {
          this.showDialogMask.emit('')
        }
      })

      ipcRenderer.send('add-connection', data)
    }
  }

  public onEnter = (event: KeyboardEvent): void => {
    if (event.which === 13) {
      this.save(event)
    }
  }
}
