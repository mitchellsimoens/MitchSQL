import { Component } from '@stencil/core'

@Component({
  shadow: true,
  styleUrl: 'app-header.scss',
  tag: 'app-header'
})
export class AppHeader {
  public render (): JSX.Element {
    return <header>
      <h1>MitchSQL v0</h1>
    </header>
  }
}
