import { Component } from '@stencil/core'

@Component({
  shadow: true,
  styleUrl: 'app-main.scss',
  tag: 'app-main'
})
export class AppMain {

  public render (): JSX.Element {
    return <div>
      <app-connections />
      <app-tabpanel />
    </div>
  }

}
