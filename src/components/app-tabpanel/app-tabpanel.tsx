import { Component, State } from '@stencil/core'
import action_query_map from './action_query_map'

@Component({
  shadow: true,
  styleUrl: 'app-tabpanel.scss',
  tag: 'app-tabpanel'
})
export class AppTabpanel {
  @State() private activeTab: any
  @State() private items: any[] = []

  public render (): JSX.Element {
    const { items } = this

    if (items.length) {
      return <div>
        <div class='tab-bar'>
          {this.renderTabs()}
        </div>
        {this.renderItems()}
      </div>
    }

    return <div>
      Tabs for querying and such here!
    </div>
  }

  public componentDidLoad (): void {
    document.addEventListener('handleTab', this.onTab)
  }

  public componentDidUnload (): void {
    document.removeEventListener('handleTab', this.onTab)
  }

  private onTab = (e: CustomEvent): void => {
    const { detail } = e
    const { action, config } = detail
    const { connection, table } = config
    let tab

    const query = action_query_map(action, connection.type, {
      table
    })

    if (query != null) {
      tab = {
        config,
        query,
        title: table.name,
        type: 'query'
      }
    }

    if (tab) {
      this.items = [
        ...this.items,
        tab
      ]

      this.activeTab = tab
    }
  }

  private renderTabs (): JSX.Element[] {
    const { activeTab, items } = this

    return items.map((item: any, index: number) =>
      <div
        class={activeTab === item || (!activeTab && !index) ? 'active' : ''}
        onClick={this.onTabClick.bind(this, item)}
        >
        {item.title}
      </div>
    )
  }

  private renderItems (): JSX.Element {
    const { activeTab, items } = this
    const active = items.find((item: any, index: number): boolean => activeTab === item || (!activeTab && !index))

    if (active.type === 'query') {
      return <tab-query
        {...active.config}
        query={active.query}
        />
    }

    return <div>{active.query}</div>
  }

  private onTabClick(item): void {
    this.activeTab = item
  }
}
