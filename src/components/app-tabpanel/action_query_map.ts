/**
 * This is a mapping of actions to queries. The propeties are
 * the actions, the value is the queries. If the query is a string,
 * it will be used for all connection types. You can provide an
 * object to have different queries for each connection type
 * such as:
 *
 *     {
 *       FOO_ACTION: {
 *         mysql: 'SELECT * FROM {table.name};
 *        pg: 'SELECT * FROM {table.name};
 *       }
 *     }
 *
 * The queries can use certain values that will be replaced. These
 * are signified by curly braces like above `{table.name}` will be
 * looked up and replaced.
 */
const map = {
  SELECT_FIRST_100_ROWS: 'SELECT * FROM {table.name} LIMIT 100;'
}

const re = /({[^{}]+})/gm

const getValue = (object: any, property: string): string => {
  let obj = object;
  const parts = property
    /**
     * Splits the property for dot and bracket notations:
     *
     *  - foo.bar
     *  - foo[1]
     *  - foo[0].bar
     *  - foo[0][1]
     *  - foo[0][3].bar[1].baz
     *  - foo['bar'][1].baz
     *  - foo.bar[0]["baz"]
     *
     * For brackets, it will split on the bracket so the
     * part is whatever is between the brackets (number for
     * array, even text for object).
     */
    .split(/\['?"?|'?"?\]\.?\[?'?"?|\./)
    /**
     * Removes any empty strings that can occur
     * with bracket notation.
     */
    .filter(Boolean);

  while (obj && parts.length) {
    const part = parts.shift()

    obj = obj[ part ]
  }

  return obj
}

export default (action, type, values): string | void => {
  let query = map[action]

  if (query) {
    if (typeof query === 'object') {
      query = query[type]
    }

    if (values) {
      query = query.replace(re, (match: string): string =>
        getValue(
          values,
          match.substr(1, match.length - 2)
        ) || match
      )
    }
  }

  return query
}
