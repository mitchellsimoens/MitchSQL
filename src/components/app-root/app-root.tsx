declare global {
  // tslint:disable-next-line:interface-name
  interface Window { require: any; }
}

import { Component } from '@stencil/core'
const { remote } = window.require('electron')

const { app: { isPackaged }, Menu, MenuItem } = remote

import 'ionicons'

@Component({
  shadow: true,
  styleUrl: 'app-root.scss',
  tag: 'app-root'
})
export class AppRoot {

  public render(): JSX.Element {
    const onContextMenu = this.createContextMenuHandler()

    return <div onContextMenu={onContextMenu}>
      <app-header />
      <app-main />
    </div>
  }

  private createContextMenuHandler (): (e: MouseEvent) => void {
    if (isPackaged) {
      return
    }

    let rightClickPosition

    const menu = new Menu()
    const menuItem = new MenuItem({
      click (): void {
        const win = remote.getCurrentWindow()

        win.focus()
        win.devToolsWebContents.focus()

        win.inspectElement(rightClickPosition.x, rightClickPosition.y)
      },
      label: 'Inspect Element'
    })

    menu.append(menuItem)

    return (e: MouseEvent): void => {
      e.preventDefault()

      rightClickPosition = { x: e.x, y: e.y }

      menu.popup({
        window: remote.getCurrentWindow()
      })
    }
  }
}
