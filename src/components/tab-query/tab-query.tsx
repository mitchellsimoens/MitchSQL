import { Component, Element, Prop, State } from '@stencil/core'
const { ipcRenderer } = window.require('electron')

@Component({
  shadow: true,
  styleUrl: 'tab-query.scss',
  tag: 'tab-query'
})
export class TabQuery {
  @Element() private el: HTMLElement

  @Prop() public alias: string
  @Prop() public connection: any
  @Prop() public database: any
  @Prop() public query: string
  @Prop() public table: any
  @Prop() public type: string

  @State() private innerQuery: string
  @State() private result: any

  public componentDidLoad(): void {
    const field = this.el.shadowRoot.querySelector('textarea')

    setImmediate(() => {
      field.focus()

      this.run()
    })
  }

  public render (): JSX.Element {
    return <div>
      <div class='top-dock'>
        <div onClick={this.run}>
          <ion-icon name='flash' />
          Run
        </div>
      </div>
      {this.renderQuery()}
      {this.renderResult()}
    </div>
  }

  private renderQuery (): JSX.Element {
    return <textarea onInput={this.onQueryChange}>{this.innerQuery || this.query}</textarea>
  }

  private renderResult (): JSX.Element {
    const { result } = this

    if (result) {
      if (result.success) {
        const { data, meta } = result
        const { profile, rowCount = data.length } = meta

        if (Array.isArray(data)) {
          return <div class='result'>
            {this.renderArray(data)}
            <div class='bottom-dock'>Finished with {rowCount} rows{profile ? ` in {profile.duration} seconds` : ''}.</div>
          </div>
        }

        return <div class='result'>Results here</div>
      }

      return <div class='result'>Something happened!</div>
    }

    return null
  }

  private onQueryChange = (event: any) => {
    const { value } = event.target

    this.innerQuery = value
  }

  private run = () => {
    const query = this.innerQuery || this.query

    if (query) {
      const { alias, database: { name: database }, table: { name: table }, type } = this

      // need to have IDs for transactions
      ipcRenderer.once('run-database-query-result', this.onQuery)
      ipcRenderer.send('run-database-query', { alias, database, query, table, type })
    }
  }

  private onQuery = (_event: Event, result: any) => {
    this.result = result
  }

  private renderArray (data: any[]): JSX.Element {
    const first = data[ 0 ]
    const rows = []

    if (first) {
      const cells = []

      Object
        .keys(first)
        .forEach((key: string) =>
          cells.push(<th>{key}</th>)
        )

        rows.push(
          <thead>
            <tr>
              {cells}
            </tr>
          </thead>
        )
    }

    data.forEach((item: any, index: number) => {
      const cells = []

      Object
        .keys(item)
        .forEach((key: string) =>
          cells.push(<td>{item[ key ]}</td>)
        )

      rows.push(
        <tr class={index % 2 ? 'alt-row' : ''}>
          {cells}
        </tr>
      )
    })

    return <table>{rows}</table>
  }
}
