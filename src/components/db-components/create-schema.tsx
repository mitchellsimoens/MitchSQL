import { Component, Event, EventEmitter, Prop, State } from '@stencil/core'
import { Event as ElectronEvent } from 'electron'
const { ipcRenderer, remote } = window.require('electron')

const { app: { isPackaged }, Menu, MenuItem } = remote

@Component({
  shadow: true,
  styleUrl: 'create-schema.scss',
  tag: 'create-schema'
})
export class CreateSchema {
  @Event() public databaseEvent: EventEmitter
  @Event() public showDialog: EventEmitter
  @Event() public showDialogMask: EventEmitter

  @Prop() public alias: string
  @Prop() public type: string

  @State() private nameValue: string

  private onContextMenu = (e: MouseEvent): void => {
    e.preventDefault()
    e.stopPropagation()

    const items = []

    if (!isPackaged) {
      items.push(
        new MenuItem({ type: 'separator' }),
        new MenuItem({
          click: this.inspectElement.bind(this, e),
          label: 'Inspect Element'
        })
      )
    }

    const menu = Menu.buildFromTemplate(items)

    menu.popup({
      window: remote.getCurrentWindow()
    })
  }

  public render (): JSX.Element {
    return <form onContextMenu={this.onContextMenu}>
      <label>
        <div>Alias</div>
        <input type='text' name='name' value={this.nameValue} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>
      <div class='toolbar'>
        <button type='button' onClick={this.save}>Save</button>
        <button type='button' onClick={this.hide}>Cancel</button>
      </div>
    </form>
  }

  private hide = (): void => {
    this.showDialog.emit(false)
  }

  private save = (event: KeyboardEvent | MouseEvent): void => {
    event.preventDefault()

    const { alias, nameValue, type } = this

    if (nameValue) {
      this.showDialogMask.emit('Saving...')

      ipcRenderer.once('add-schema-result', (_event: ElectronEvent, result: any) => {
        if (result.success) {
          this.databaseEvent.emit({
            alias, type,
            action: 'add',
            name: nameValue
          });

          this.showDialog.emit(false)
        } else {
          this.showDialogMask.emit('')
        }
      })

      ipcRenderer.send('add-schema', {
        alias, type,
        name: nameValue
      })
    }
  }

  private handleChange = (event: any): void => {
    const { name, value } = event.target

    this[ `${name}Value` ] = value
  }

  private inspectElement = (e): void => {
    const win = remote.getCurrentWindow()

    win.focus()
    win.devToolsWebContents.focus()

    win.inspectElement(e.x, e.y)
  }

  public onEnter = (event: KeyboardEvent): void => {
    if (event.which === 13) {
      this.save(event)
    }
  }
}
