import { Component, Event, EventEmitter, Prop, State } from '@stencil/core'
import { Event as ElectronEvent } from 'electron'
const { ipcRenderer, remote } = window.require('electron')

const { app: { isPackaged }, Menu, MenuItem } = remote

@Component({
  shadow: true,
  styleUrl: 'create-table.scss',
  tag: 'create-table'
})
export class CreateSchema {
  @Event() public tableEvent: EventEmitter
  @Event() public showDialog: EventEmitter
  @Event() public showDialogMask: EventEmitter

  @Prop() public alias: string
  @Prop() public database: string
  @Prop() public type: string

  @State() private columns: any[] = []
  @State() private nameValue: string
  @State() private newColumnDatatype: string
  @State() private newColumnDefault: string
  @State() private newColumnName: string

  @State() private newColumnAI: boolean
  @State() private newColumnBIN: boolean
  @State() private newColumnG: boolean
  @State() private newColumnNN: boolean
  @State() private newColumnPK: boolean
  @State() private newColumnUN: boolean
  @State() private newColumnUQ: boolean
  @State() private newColumnZF: boolean

  private onContextMenu = (e: MouseEvent): void => {
    e.preventDefault()
    e.stopPropagation()

    const items = []

    if (!isPackaged) {
      items.push(
        new MenuItem({ type: 'separator' }),
        new MenuItem({
          click: this.inspectElement.bind(this, e),
          label: 'Inspect Element'
        })
      )
    }

    const menu = Menu.buildFromTemplate(items)

    menu.popup({
      window: remote.getCurrentWindow()
    })
  }

  public render (): JSX.Element {
    return <form onContextMenu={this.onContextMenu}>
      <label>
        <div>Table Name</div>
        <input type='text' name='name' value={this.nameValue} onInput={this.handleChange} onKeyDown={this.onEnter} required={true} />
      </label>
      <hr />
      { this.renderColumns() }
      <div class='toolbar'>
        <button type='button' onClick={this.save}>Save</button>
        <button type='button' onClick={this.hide}>Cancel</button>
      </div>
    </form>
  }

  private renderColumns (): JSX.Element[] {
    return [
      <div>Columns: ({this.columns.length})</div>,
      this.renderColumnTable(),
      <hr />,
      <div>Add Column:</div>,
      <label>
        <div>Name:</div>
        <input type='text' name='name' value={this.newColumnName} onInput={this.handleColumnInput} onKeyDown={this.onColumnInputEnter} required={true} />
      </label>,
      <label>
        <div>Datatype:</div>
        <input type='text' name='datatype' value={this.newColumnDatatype} onInput={this.handleColumnInput} onKeyDown={this.onColumnInputEnter} required={true} />
      </label>,
      <label>
        <div>Default:</div>
        <input type='text' name='default' value={this.newColumnDefault} onInput={this.handleColumnInput} onKeyDown={this.onColumnInputEnter} />
      </label>,
      <label class='inline'>
        <input type='checkbox' name='PK' checked={this.newColumnPK} onChange={this.handleColumnCheck} />
        <div>PK</div>
      </label>,
      <label class='inline'>
        <input type='checkbox' name='NN' checked={this.newColumnNN} onChange={this.handleColumnCheck} />
        <div>NN</div>
      </label>,
      <label class='inline'>
        <input type='checkbox' name='UQ' checked={this.newColumnUQ} onChange={this.handleColumnCheck} />
        <div>UQ</div>
      </label>,
      <label class='inline'>
        <input type='checkbox' name='BIN' checked={this.newColumnBIN} onChange={this.handleColumnCheck} />
        <div>BIN</div>
      </label>,
      <label class='inline'>
        <input type='checkbox' name='UN' checked={this.newColumnUN} onChange={this.handleColumnCheck} />
        <div>UN</div>
      </label>,
      <label class='inline'>
        <input type='checkbox' name='ZF' checked={this.newColumnZF} onChange={this.handleColumnCheck} />
        <div>ZF</div>
      </label>,
      <label class='inline'>
        <input type='checkbox' name='AI' checked={this.newColumnAI} onChange={this.handleColumnCheck} />
        <div>AI</div>
      </label>,
      <label class='inline'>
        <input type='checkbox' name='G' checked={this.newColumnG} onChange={this.handleColumnCheck} />
        <div>G</div>
      </label>
    ]
  }

  private renderColumnTable (): JSX.Element {
    return this.columns.length
    ? <table>
      <thead>
        <tr>
          <td>Name</td>
          <td>Datatype</td>
          <td>Default</td>
          <td>PK</td>
          <td>NN</td>
          <td>UQ</td>
          <td>BIN</td>
          <td>UN</td>
          <td>ZF</td>
          <td>AI</td>
          <td>G</td>
        </tr>
      </thead>
      {this.columns.map((column: any) =>
        <tr>
          <td>{column.name}</td>
          <td>{column.datatype}</td>
          <td>{column.default}</td>
          <td>{column.pk ? 'Y' : ''}</td>
          <td>{column.nn ? 'Y' : ''}</td>
          <td>{column.uq ? 'Y' : ''}</td>
          <td>{column.bin ? 'Y' : ''}</td>
          <td>{column.un ? 'Y' : ''}</td>
          <td>{column.zf ? 'Y' : ''}</td>
          <td>{column.ai ? 'Y' : ''}</td>
          <td>{column.g ? 'Y' : ''}</td>
        </tr>
      )}
    </table>
    : <div>No columns</div>
  }

  private hide = (): void => {
    this.showDialog.emit(false)
  }

  private save = (event: KeyboardEvent | MouseEvent): void => {
    event.preventDefault()

    const { alias, database, nameValue, type } = this

    if (nameValue) {
      this.showDialogMask.emit('Saving...')

      ipcRenderer.once('add-table-result', (_event: ElectronEvent, result: any) => {
        if (result.success) {
          this.tableEvent.emit({
            alias, database, type,
            action: 'add',
            name: nameValue
          });

          this.showDialog.emit(false)
        } else {
          this.showDialogMask.emit('')
        }
      })

      ipcRenderer.send('add-table', {
        alias, database, type,
        columns: this.columns,
        name: nameValue
      })
    }
  }

  private handleChange = (event: any): void => {
    const { name, value } = event.target

    this[ `${name}Value` ] = value
  }

  private inspectElement = (e): void => {
    const win = remote.getCurrentWindow()

    win.focus()
    win.devToolsWebContents.focus()

    win.inspectElement(e.x, e.y)
  }

  public onEnter = (event: KeyboardEvent): void => {
    if (event.which === 13) {
      this.save(event)
    }
  }

  private handleColumnInput = (event: any): void => {
    const { name, value } = event.target

    if (name === 'name') {
      this.newColumnName = value
    } else if (name === 'datatype') {
      this.newColumnDatatype = value
    }
  }

  private handleColumnCheck = (event: any): void => {
    const { checked, name } = event.target
    const prop = `newColumn${name}`

    this[ prop ] = checked
  }

  private onColumnInputEnter = (event: KeyboardEvent): void => {
    if (event.which === 13) {
      event.preventDefault()

      // TODO more validation, is datatype valid? is there a conflicting column?
      if (this.newColumnDatatype && this.newColumnName) {
        const columns = this.columns.slice()

        columns.push({
          ai: this.newColumnAI,
          bin: this.newColumnBIN,
          datatype: this.newColumnDatatype,
          default: this.newColumnDefault,
          g: this.newColumnG,
          name: this.newColumnName,
          nn: this.newColumnNN,
          pk: this.newColumnPK,
          un: this.newColumnUN,
          uq: this.newColumnUQ,
          zf: this.newColumnZF
        })

        this.columns = columns

        this.newColumnDatatype = this.newColumnDefault = this.newColumnName = ''

        this.newColumnAI = this.newColumnBIN = this.newColumnG =
          this.newColumnNN = this.newColumnPK = this.newColumnUN =
          this.newColumnUQ = this.newColumnZF = false
      }
    }
  }
}
