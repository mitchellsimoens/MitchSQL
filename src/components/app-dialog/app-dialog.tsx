import { Component, Event, EventEmitter, Method, Prop, State } from '@stencil/core'

@Component({
  shadow: true,
  styleUrl: 'app-dialog.scss',
  tag: 'app-dialog'
})
export class AppDialog {
  @Event() public showDialog: EventEmitter

  @Prop() public items: JSX.Element[]
  @Prop() public mask: string
  @Prop() public size: string = 'small'
  @Prop() public title: string

  @State() private shown: boolean = false

  public render(): JSX.Element {
    if (!this.shown) {
      return null
    }

    return <div onClick={this.onMaskClick}>
      <div class={`inner ${this.size}`}>
        {
          this.title
            ? <h1>{this.title}</h1>
            : null
        }
        {
          this.mask
            ? <div class='mask shown'>
              <div>{this.mask}</div>
            </div>
            : null
        }
        { this.items }
      </div>
    </div>
  }

  public componentDidLoad (): void {
    document.addEventListener('showDialog', this.onShowDialog)
  }

  public componentDidUnload (): void {
    document.removeEventListener('showDialog', this.onShowDialog)
  }

  private onShowDialog = (e: CustomEvent) => {
    const { detail } = e

    if (detail !== this.shown) {
      if (detail) {
        this.show()
      } else {
        this.hide()
      }
    }
  }

  @Method() public hide (): void {
    if (this.shown) {
      this.shown = false

      this.showDialog.emit(false)
    }
  }

  @Method() public show (): void {
    if (!this.shown) {
      this.shown = true

      this.showDialog.emit(true)
    }
  }

  @Method() public toggle(): void {
    if (this.shown) {
      this.hide()
    } else {
      this.show()
    }
  }

  private onMaskClick = (event): void => {
    const isMask = !event.path.find((item: Element): boolean => item.classList && item.classList.contains('inner'))

    if (isMask) {
      this.hide()
    }
  }
}
