const mysql = require('mysql')

class MySQL {
  constructor (data) {
    this.data = data

    this.connection = mysql.createConnection({
      database: data.database,
      host: data.host,
      password: data.password,
      port: data.port,
      user: data.username,

      multipleStatements: true
    })
  }

  check () {
    return new Promise((resolve, reject) => {
      const { connection } = this

      connection.connect((error) =>
        error
          ? reject(error)
          : connection.end(() => resolve())
      )
    })
  }

  createSchema (name) {
    return this.query(`CREATE DATABASE ${name};`);
  }

  dropSchema (name) {
    return this.query(`DROP DATABASE ${name};`);
  }

  getDatabases () {
    return this.query('SELECT SCHEMA_NAME as name FROM INFORMATION_SCHEMA.SCHEMATA ORDER BY name;')
  }

  getTables (database) {
    return this.query(`SELECT table_name as name FROM information_schema.tables where table_schema='${database}';`)
  }

  query (sql) {
    return new Promise((resolve, reject) => {
      const { connection } = this

      connection.connect((error) => {
        if (error) {
          reject(error)
        } else {
          if (Array.isArray(sql)) {
            sql = sql.join('')
          }

          if (sql.substr(sql.length - 1) !== ';') {
            sql = `${sql};`
          }

          sql = `SET profiling = 1;
${sql}
SELECT SUM(duration) as duration FROM information_schema.profiling GROUP BY query_id ORDER BY query_id DESC LIMIT 1;
SET profiling = 0;`;

          connection.query(sql, (error, result, fieldsArr) => {
            if (error) {
              reject(error)
            } else {
              const [ , data, [ profile ] ] = result
              const [ , fields ] = fieldsArr

              resolve({
                data,
                meta: {
                  fields,
                  profile
                },
                success: true
              })
            }
          })
        }
      })
    })
  }
}

module.exports = MySQL
