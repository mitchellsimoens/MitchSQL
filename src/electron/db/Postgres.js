const { Client } = require('pg')

class Postgres {
  constructor (data) {
    this.data = data

    this.connection = new Client({
      database: data.database,
      host: data.host,
      password: data.password,
      port: data.port,
      user: data.username
    })
  }

  check () {
    const { connection } = this

    return connection
      .connect()
      .then(() => connection.end())
  }

  createSchema (name) {
    return this.query(`CREATE DATABASE ${name};`);
  }

  dropSchema (name) {
    return this.query(`DROP DATABASE ${name};`);
  }

  getDatabases () {
    return this.query(
      `SELECT datname as name
        FROM pg_database
        WHERE datistemplate = false ORDER BY name;`
    )
  }

  getTables () {
    return this.query(
      `SELECT tablename as name FROM pg_tables WHERE schemaname = 'public' ORDER BY name;`
    )
  }

  query (sql) {
    const { connection } = this

    if (Array.isArray(sql)) {
      sql = sql.join('')
    }

    // TODO need duration

    return connection
      .connect()
      .then(() => connection.query(sql))
      .then(ret => ({
        // what does ret look like on a non-SELECT like INSERT or DELETE?
        data: ret.rows,
        meta: {
          ...ret,
          rows: undefined
        },
        success: true
      }))
      .then(ret =>
        connection
          .end()
          .then(() => ret)
      )
  }
}

module.exports = Postgres
