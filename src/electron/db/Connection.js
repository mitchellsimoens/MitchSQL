const config = require('../config')
const types = require('./types')

class Connection {
  add (data) {
    const connections = config.get('connections', {})

    let map = connections[ data.type ]

    if (!map) {
      map = connections[ data.type ] = {}
    }

    if (map[ data.alias ]) {
      return Promise.reject(new Error('Alias already exists!'))
    } else {
      // TODO encrypt password?
      map[ data.alias ] = data

      config.set({
        connections
      })

      return Promise.resolve()
    }
  }

  check (data) {
    const cls = types.get(data.type)

    if (cls) {
      const instance = new cls(data)

      return instance
        .check()
        .then(() => ({
          success: true
        }))
        .catch(() => ({
          success: false
        }))
    }

    return Promise.resolve({
      success: false
    })
  }

  cleanse (obj) {
    return {
      ...obj,
      password: undefined
    }
  }

  createSchema (type, alias, name) {
    const cls = types.get(type)

    if (cls) {
      const data = this.getConnection(type, alias)
      const instance = new cls(data)

      return instance
        .createSchema(name)
        .then(() => ({
          success: true
        }))
        .catch(() => ({
          success: false
        }))
    }

    return Promise.resolve({
      success: false
    })
  }

  dropSchema (type, alias, name) {
    const cls = types.get(type)

    if (cls) {
      const data = this.getConnection(type, alias)
      const instance = new cls(data)

      return instance
        .dropSchema(name)
        .then(() => ({
          success: true
        }))
        .catch(() => ({
          success: false
        }))
    }

    return Promise.resolve({
      success: false
    })
  }

  get () {
    const connections = config.get('connections', {})
    const ret = {}

    Object
      .keys(connections)
      .forEach(type => {
        const types = connections[ type ]

        ret[ type ] = {}

        Object
          .keys(types)
          .forEach(alias => {
            ret[ type ][ alias ] = this.cleanse(types[ alias ])
          })
      })

    return Promise.resolve(ret)
  }

  getConnection (type, alias) {
    const connections = config.get('connections', {})
    const typeObj = connections[ type ]

    if (typeObj) {
      return typeObj[ alias ]
    }
  }

  getDatabases (type, alias) {
    const cls = types.get(type)

    if (cls) {
      const data = this.getConnection(type, alias)
      const instance = new cls(data)

      return instance
        .getDatabases()
        .catch(error => ({
          error: error.message,
          success: false
        }))
    }

    return Promise.reject(new Error('Type unsupported'))
  }

  getTables (type, alias, database) {
    const cls = types.get(type)

    if (cls) {
      const data = this.getConnection(type, alias)
      const instance = new cls(data)

      return instance
        .getTables(database)
        .catch(error => ({
          error: error.message,
          success: false
      }))
    }

    return Promise.reject(new Error('Type unsupported'))
  }

  query (type, alias, database, sql) {
    const cls = types.get(type)

    if (cls) {
      const data = this.getConnection(type, alias)
      const instance = new cls({
        ...data,
        database
      })

      return instance.query(sql)
    }

    return Promise.reject(new Error('Type unsupported'))
  }
}

module.exports = new Connection()
