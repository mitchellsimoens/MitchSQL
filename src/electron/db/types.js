const mysql = require('./MySQL')
const postgres = require('./Postgres')

const types = {
  mysql,
  pg: postgres,
  postgres,

  get (type) {
    return types[ type ]
  }
}

module.exports = types
