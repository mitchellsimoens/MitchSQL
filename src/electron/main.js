const { app, BrowserWindow, ipcMain } = require('electron')
const Connection = require('./db/Connection')
const config = require('./config')

const { isPackaged } = app
const isMac = process.platform === 'darwin'

const defaultWindowOptions = {
  show: false
}

let mainWindow

const createWindow = () => {
  const bounds = config.get('winBounds')

  const opts = {
    ...defaultWindowOptions,
    ...bounds
  }

  mainWindow = new BrowserWindow(opts)

  mainWindow.loadURL('http://localhost:3333')

  mainWindow.once('ready-to-show', mainWindow.show)

  if (!isPackaged) {
    mainWindow.webContents.on('devtools-opened', () => {
      mainWindow.focus()

      setImmediate(() => mainWindow.focus())
    })

    mainWindow.webContents.openDevTools({
      mode: 'detach'
    })
  }

  mainWindow.on('close', () => {
    config.set('winBounds', mainWindow.getBounds())
  })

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

const quit = () => app.quit()

app.on('ready', createWindow)

app.on('window-all-closed', () => !isMac && quit())

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (!mainWindow) {
    createWindow()
  }
})

const handleResponse = (event, channel, ret) => {
  if (event && event.sender) {
    event.sender.send(channel, ret)
  } else if (ret instanceof Error) {
    throw ret
  } else {
    return ret
  }
}

const onAddConnection = (event, data) => {
  Connection
    .check(data)
    .then((ret) => {
      return Connection
        .add(data)
        .then(() => ret)
    })
    .then(handleResponse.bind(this, event, 'add-connect-result'))
    .catch((error) => ({
      error,
      success: false
    }))
}

const onAddSchema = (event, { alias, name, type }) => {
  Connection
    .createSchema(type, alias, name)
    .then(handleResponse.bind(this, event, 'add-schema-result'))
    .catch((error) => ({
      error,
      success: false
    }))
}

const onDropSchema = (event, { alias, name, type }) => {
  Connection
    .dropSchema(type, alias, name)
    .then(handleResponse.bind(this, event, 'drop-schema-result'))
    .catch((error) => ({
      error,
      success: false
    }))
}

const onGetConnections = (event) => {
  Connection
    .get()
    .then(handleResponse.bind(this, event, 'get-connections-result'))
    .catch((error) => ({
      error,
      success: false
    }))
}

const onGetConnectionDatabases = (event, { alias, type }) => {
  Connection
    .getDatabases(type, alias)
    .then(handleResponse.bind(this, event, 'get-connection-databases-result'))
    .catch((error) => ({
      error,
      success: false
    }))
}

const onGetConnectionTables = (event, { alias, database, type }) => {
  Connection
    .getTables(type, alias, database)
    .then(handleResponse.bind(this, event, 'get-connection-tables-result'))
    .catch((error) => ({
      error,
      success: false
    }))
}

const onRunDatabaseQuery = (event, { alias, database, query, type }) => {
  Connection
    .query(type, alias, database, query)
    .then(handleResponse.bind(this, event, 'run-database-query-result'))
    .catch((error) => ({
      error,
      success: false
    }))
}

ipcMain.on('add-connection', onAddConnection)
ipcMain.on('add-schema', onAddSchema)
ipcMain.on('drop-schema', onDropSchema)
ipcMain.on('get-connections', onGetConnections)
ipcMain.on('get-connection-databases', onGetConnectionDatabases)
ipcMain.on('get-connection-tables', onGetConnectionTables)
ipcMain.on('run-database-query', onRunDatabaseQuery)
