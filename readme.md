# MitchSQL

I hated all the GUI apps for working with MySQL and PostgresSQL so I started working on my own.

## Getting Started

First install all the dependencies

```bash
npm install
```

Then to run:

```bash
npm start
```

To build the app for production, run:

```bash
npm run build
```

To run the unit tests once, run:

```
npm test
```

To run the unit tests and watch for file changes during development, run:

```
npm run test.watch
```
